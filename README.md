
The purpose of *alma-proxy* is to provide a safer way to access Alma's API:s (from JavaSript code in Primo, primarily). Calling Alma's API:s from JavaScript directly is less than ideal for at least two reasons: 1) the API key would be compromised, 2) the access provided is often too broad, e.g. even the Bibs API contains sensitive data. Alma-proxy protects the API keys and lets you configure more limited access to the API:s. If used along with a compatible login solution, it may also limit access based on whether the user is logged into Primo.

# Usage and configuration

Define any number of "access points" in *api_configs.yml* (see *api_configs.example.yml*) and call the API:s as per usual with `{access point name}/almaws/v1/{some API path}`. The `/almaws/v1` part may be omitted.

## Configuration directives

### allow

Specifies one or many regular expressions to explicitly state which parts (i.e. paths) of the API that may be accessed as well as by which HTTP method(s).

```
  allow: GET /almaws/v1/bibs/.*/requests/.*
```

### authenticate

If true, limits access to clients that are currently logged into Primo.

### deny

Specifies one or many regular expressions to explicitly state which parts (i.e. paths) of the API that may *not* be accessed as well as by which HTTP method(s).

```
  deny: GET /almaws/v1/bibs/.*/requests/.*
```

### hide

Replaces one or many properties in the response data with *null*. Use '|' to specify the path to some data property.

```
  hide:
    - user_request|user_primary_id
    - user_request|request_sub_type|desc
```

### internal

Marks the configuration for internal use only. You don't need this.

### key

Specifies the API key.
