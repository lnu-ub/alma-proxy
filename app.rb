# coding: utf-8

require 'sinatra/base'
require 'ex_libris_api'
require 'json'
require 'yaml'
require 'mysql2'
require 'hash_dig_and_collect'
require 'date'
require 'net/http'
require 'jwt'

class AlmaProxy < Sinatra::Base
  configure do
    set :logging, true
    api_configs = YAML.load_file('api_configs.yml')
    api = ExLibrisAPI::API.new(
      api_key: api_configs['i_bibs']['key'],
      format: 'application/json')
    set :api, api
    set :api_configs, api_configs
  end

  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
  end

  # Primo skickar OPTIONS och förväntar sig nedan svar.
  options '*' do
    response.headers['Allow'] = 'HEAD,GET,PUT,POST,DELETE,OPTIONS'
    response.headers['Access-Control-Allow-Methods'] = 'HEAD,GET,PUT,POST,DELETE,OPTIONS'    
    response.headers['Access-Control-Allow-Headers'] = 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Cache-Control, Accept, x-from-exl-api-gateway, Authorization'
    200
  end

  def output(data, params)
    if params['callback']
      content_type 'application/javascript', charset: 'utf-8'
      params['callback'].to_s + '(' + data.to_json + ')'
    else
      content_type :json, charset: 'utf-8'
      data.to_json
    end
  end

  def error(e, request)
    puts DateTime.now
    puts 'Ruby error: ' + e.inspect
    puts 'Alma API error codes: ' + e.codes.inspect if e.respond_to? :codes
    puts 'Alma API REST error: ' + e.rest_e.inspect if e.respond_to? :rest_e

    unless request.nil?
      puts 'Path: ' + request.path unless request.path.nil?
      puts 'Referer: ' + request.referer unless request.referer.nil?
    end

    puts 'Params: ' + params.inspect
    puts e.backtrace
  end

  get '/holdings/:mms_id/?' do |mms_ids|
    begin
      response.headers['Cache-Control'] = 'max-age=10'

      holdings = settings.api.x_get_holdings(
        mms_ids: mms_ids.split(','),
        extra_item_fields: %w(creation_date physical_material_type awaiting_reshelving),
        sloppy_mode: !params['sloppy_mode'].nil?
      )
      output(holdings, params)
    rescue => e
      error(e, request)
      halt 500
    end
  end

  get '/items/:mms_id/?' do |mms_ids|
    begin
      response.headers['Cache-Control'] = 'max-age=10'

      items = settings.api.x_get_items(
        mms_ids: mms_ids.split(','),
        group_by: params['group_by'],
        sort_by: params['sort_by'],
        reverse: params['reverse'],
      )
      output(items, params)
    rescue => e
      error(e, request)
      halt 500
    end
  end

  %w(get post put delete).each do |method|
    send method.to_sym, '/*/*' do |config_name, path|
      config = settings.api_configs[config_name]
      halt 400 if !config || config['internal']
      path = '/' + path
      path = '/almaws/v1' + path unless path =~ /^\/almaws\/v\d+/

      if config['max_age']
        # Be klienten cacha.
        response.headers['Cache-Control'] = 'max-age=' + config['max_age'].to_s
      end

      # Begränsa tillgång till enskilda API:er.
      # TODO: Stöd för flera mönster.
      if config['allow']
        halt 403 unless [*config['allow']].any? do |pattern|
          method + ' ' + path =~ Regexp.new(pattern, true)
        end
      elsif config['deny']
        halt 403 if [*config['deny']].any? do |pattern|
          method + ' ' + path =~ Regexp.new(pattern, true)
        end
      end

      # Begränsa åtkomst till användare inloggade i Primo.
      if config['authenticate']
        auth = request.env['HTTP_AUTHORIZATION'] or halt 400
        user_id, token = auth.split(':')

        if token.to_s.empty? || user_id.to_s.empty?
          halt 400
        end

        # Infoga användar-id på angiven plats.
        path.sub!(/ALMA_USER_ID/, user_id)

        begin
          client = Mysql2::Client.new(:host => "localhost", :database => "ils", :username => "ils_usr", :password => "DK^.vzv3FL[-P}q")
          escaped_token = client.escape(token)
          escaped_user_id = client.escape(user_id)
          results = client.query("SELECT * FROM api_tokens WHERE token='#{escaped_token}' AND almaid='#{escaped_user_id}'")

          # Svara bara om det finns en giltig api-pollett
          if results.size === 0
            halt 403
          end

        rescue => e
          puts e.inspect
          halt 500
        ensure
          client.close if client
        end
      end

      if config['jwt']
        # Bearer jwt
        auth = request.env['HTTP_AUTHORIZATION'] or halt 400
        token = auth.split(' ').last.delete_prefix('"').delete_suffix('"')

        jwks_loader = ->(options) do
          if @cache_last_update && @cache_last_update < Time.now.to_i - 300
            @cached_keys = nil
          end
          @cached_keys ||= begin
                             @cache_last_update = Time.now.to_i
                             jwks = JSON.parse(Net::HTTP.get URI('https://api-eu.hosted.exlibrisgroup.com/auth/46GSLG_VAXJO/jwks.json'))
                           end
        end

        begin
          decoded_token = JWT.decode(token, nil, true, { iss: 'Prima', verify_iss: true, verify_jti: true, algorithms: ['ES256'], jwks: jwks_loader})
          user_id = decoded_token[0]["userName"]
          
          # Infoga användar-id på angiven plats.
          path.sub!(/ALMA_USER_ID/, user_id)
        rescue  => e
          puts e.inspect
          halt 401
        end
      end

      begin
        params.delete('splat') # Skapas av Sinatra och stör API:et
        format = params['format'] || 'application/json'
        content_type format, charset: 'utf-8'
        api = ExLibrisAPI::API.new(api_key: config['key'], format: format)

        # Hiding properties is currently only supported for JSON data.
        if config['hide'] && format =~ /json/i
          if method == 'post' || method == 'put'
              body = request.body.read
              data = api.send method.to_sym, path, body, params, { parse: true }
          else
              data = api.send method.to_sym, path, params, { parse: true }
          end

          [*config['hide']].each do |prop|
            keys = prop.split '|'
            if keys.size == 1
              data[keys[0]] = nil
            elsif keys.size > 1
              leaf = keys.pop
              data.dig_and_collect(*keys).flatten.each { |node| node.store(leaf, nil) }
            end
          end
          data.to_json
        else
          if method == 'post' || method == 'put'
            body = request.body.read
            api.send method.to_sym, path, body, params, { parse: false }
          else
            api.send method.to_sym, path, params, { parse: false }
          end
        end
      rescue => e
        error(e, request)
        halt 500
      end
    end
  end
end
